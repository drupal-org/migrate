Drupal migrate/supported README.txt
==============================================================================

This directory contains support files for the migrate module.

See docs/migrate_hook_docs.php to see how you can create your own
support file.

The name of the support file is tied to the module providing the
functionality. For example, if your content type "Foo thingy" is provided
by your "foo.module" then you'll need to add a "foo.inc" file inside this
directory.

Please consider contacting the maintainer or posting an issue on
http://drupal.org/project/migrate if you create a support file for a
content type that is widely used.

